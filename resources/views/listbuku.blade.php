<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
    </head>
    <body class="antialiased">
    <form method="post" action="/cari">
        @csrf
        <input type="text" name="judul"/><br/><br/>
        <input type="submit" value="CARI BUKU"/>
    </form>

    Daftar Buku :<br/>
    <table border='1' style='width:50%' >
    <th style='width:10%'>ID</th><th style='width:60%'> Judul </th><th></th></tr>
    @foreach ($list as $buku)
        <tr> <td>{{ $buku->id }}</td><td>{{ $buku->judul }}</td><td style='text-align: center;'><a href='/buku/{{ $buku->id }}'> pinjam </a></td></tr>
    @endforeach
</table>
    </body>
</html>
