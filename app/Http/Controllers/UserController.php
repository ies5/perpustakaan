<?php

namespace App\Http\Controllers;

use App\Models\Buku;
use App\Models\Peminjaman;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    protected $service;

    public function __construct(PerpustakaanService $bukuServices)
    {
        $this->service = $bukuServices;
    }

    public function login(Request $request)
    {
        $email = $request['email'];
        $password = $request['password'];
        $user = $this->service->getUserByEmail($email);
        if (!isset($user)) {
            return view('login', ['pesan' => 'email salah']);
        }
        if ($user->password != $password) {
            return view('login', ['pesan' => 'password salah']);
        }
        $request->session()->put('userId', $user->userId);        
        $list = $this->service->getPeminjamanByUser($user->userId);
        return view('beranda', ['user' => $user, 'list' => $list]);
    }

    public function registrasi(Request $request)
    {
        return view('login');
    }

    public function beranda(Request $request)
    {
        $userId = $request->session()->get('userId', '1');
        $user = $this->service->getUserById($userId);
        $list = $this->service->getPeminjamanByUser($userId);
        return view('beranda', ['user' => $user, 'list' => $list]);
    }
}
