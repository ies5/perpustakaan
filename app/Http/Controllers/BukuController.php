<?php

namespace App\Http\Controllers;

use App\Models\Buku;
use App\Models\Peminjaman;
use Carbon\Carbon;
use Illuminate\Http\Request;

class BukuController extends Controller
{
    protected $service;

    public function __construct(PerpustakaanService $bukuServices)
    {
        $this->service = $bukuServices;
    }

    public function index()
    {
        $list = Buku::all();
        return view('listbuku', ['list' => $list]);
    }
    
    public function cari(Request $request)
    {
        $userId = $request->session()->get('userId', '1');
        $judul = $request['judul'];
        $list = $this->service->getBukuByJudul($judul);
        return view('pencarian', ['userId'=>$userId,'list' => $list]);
    }

    public function pinjam(Request $request)
    {
        $bukuId = $request['bukuId'];
        $userId = $request->session()->get('userId', '1');
        $hari = $request['hari'];
        if ($hari > 3) {
            $buku = $this->service->getBukuById($bukuId);
            $userId = $request->session()->get('userId', '1');
            return view('peminjaman', ['buku' => $buku, 'userId' => $userId, 'pesan'=>'lama peminjaman maksimal 3 hari']);
        }
        $this->service->savePeminjaman($userId, $bukuId, $hari);
        $list = $this->service->getPeminjamanByUser($userId);        
        return view('daftar_pinjam', ['list' => $list]);
    }

    public function getBuku($id, Request $request)
    {
        $buku = $this->service->getBukuById($id);
        $userId = $request->session()->get('userId', '1');
        return view('peminjaman', ['buku' => $buku, 'userId' => $userId]);
    }
}
