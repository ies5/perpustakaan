<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/login',function () {
    return view('login');
});
Route::post('/login',[\App\Http\Controllers\UserController::class,'login']);
Route::get('/registrasi',function () {
    return view('registrasi');
});
Route::post('/cari',[\App\Http\Controllers\BukuController::class,'cari']);
Route::get('/buku/{id}',[\App\Http\Controllers\BukuController::class,'getBuku'])->name('buku');
Route::post('/pinjam',[\App\Http\Controllers\BukuController::class,'pinjam']);
Route::get('/beranda',[\App\Http\Controllers\UserController::class,'beranda']);
Route::get('/list',[\App\Http\Controllers\BukuController::class,'index']);
