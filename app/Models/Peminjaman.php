<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Peminjaman
 *
 * @method static \Illuminate\Database\Eloquent\Builder|Peminjaman newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Peminjaman newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Peminjaman query()
 * @mixin \Eloquent
 * @property int $id
 * @property int|null $bukuId
 * @property int|null $userId
 * @property int|null $hari
 * @property string|null $tglPinjam
 * @method static \Illuminate\Database\Eloquent\Builder|Peminjaman whereBukuId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Peminjaman whereHari($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Peminjaman whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Peminjaman whereTglPinjam($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Peminjaman whereUserId($value)
 */
class Peminjaman extends Model
{
    use HasFactory;

    protected $table = 'peminjaman';
    public $timestamps = false;

    public function buku()
    {
        return $this->belongsTo(Buku::class, 'bukuId');
    }
}
