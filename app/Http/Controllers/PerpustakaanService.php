<?php
namespace App\Http\Controllers;
use App\Models\Buku;
use App\Models\Peminjaman;
use App\Models\User;
use Carbon\Carbon;
class PerpustakaanService
{
    public function getBukuByJudul($keyword)
    {
        $list = Buku::whereRaw("UPPER(judul) LIKE '%" . strtoupper($keyword) . "%'")->get();
        return $list;
    }

    public function getBukuById($id)
    {
        $buku = Buku::find($id);        
        return $buku;
    }

    public function getPeminjamanByUser($userId)
    {
        $list = Peminjaman::where('userId', '=', $userId)->with('buku')->get();
        return $list;
    }

    public function savePeminjaman($userId, $bukuId, $hari)
    {
        $pinjam = new Peminjaman();
        $pinjam->userId = $userId;
        $pinjam->bukuId = $bukuId;
        $pinjam->hari = $hari;
        $pinjam->tglPinjam = Carbon::now();
        $pinjam->save();
    }

    public function getUserByEmail($email)
    {
        $user = User::whereEmail($email)->first();
        return $user;
    }
    public function getUserById($userId)
    {
        $user = User::find($userId);
        return $user;
    }
}