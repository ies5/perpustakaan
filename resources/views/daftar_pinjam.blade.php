<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
    </head>
    <body class="antialiased">
    Daftar Buku Yang Dipinjam :<br/>
    <ol>
        @foreach ($list as $pinjam)
            <li>Buku {{ $pinjam->buku->judul }}, lama peminjaman {{ $pinjam->hari }} hari</li>
        @endforeach
    </ol>

    </body>
</html>
